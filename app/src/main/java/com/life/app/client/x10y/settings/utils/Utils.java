package com.life.app.client.x10y.settings.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

/*
 * Created by chkalog on 7/9/2016.
 */
public class Utils {

    public static String getMonthName(Context ctx, int month){
        String months[] = ctx.getResources().getStringArray(R.array.months);
        return months[month];
    }

    public static void showSnackBar(CoordinatorLayout coordinatorLayout, String msg){
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    /**
     * <strong>public void showAToast (String st)</strong></br>
     * this little method displays a toast on the screen.</br>
     * it checks if a toast is currently visible</br>
     * if so </br>
     * ... it "sets" the new text</br>
     * else</br>
     * ... it "makes" the new text</br>
     * and "shows" either or
     * @param st the string to be toasted
     */

    public static void showAToast (Toast toast, Context theContext, String st){ //"Toast toast" is declared in the class
        try{ toast.getView().isShown();     // true if visible
            toast.setText(st);
        } catch (Exception e) {         // invisible if exception
            toast = Toast.makeText(theContext, st, Toast.LENGTH_SHORT);
        }
        toast.show();  //finally display it
    }

    /**
     * Converts the array of bytes into a UUID and returns the generated UUID object
     *
     * @param   uuid    an array of bytes
     * @return  the converted UUID object
     */
    public static UUID getUUIDFromByte(byte[] uuid){
        ByteBuffer bb = ByteBuffer.wrap(uuid, 0, 16);
        bb.order(ByteOrder.BIG_ENDIAN);
        long firstLong = bb.getLong();
        long secondLong = bb.getLong();
        return new UUID(firstLong, secondLong);
    }

    /**
     * Converts the array of bytes into an int and returns the generated int
     *
     * @param   b    an array of bytes
     * @return  the converted int
     */
    public static int byteArrayToInt(byte[] b){
        return   b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }

    /**
     * Converts an int to an array of bytes and returns the exported array
     *
     * @param   a    an int
     * @return  the converted byte array
     */
    public static byte[] intToByteArray(int a){
        return new byte[] {
                (byte) ((a >> 24) & 0xFF),
                (byte) ((a >> 16) & 0xFF),
                (byte) ((a >> 8) & 0xFF),
                (byte) (a & 0xFF)
        };
    }

    public static boolean isEmpty(String str){
        return (str != null && !str.trim().isEmpty());
    }

    /** Open another app.
     * @param context current Context, like Activity, App, or Service
     * @return true if likely successful, false if unsuccessful
     */
    public static boolean openLoginPage(Context context, String email) {
        PackageManager manager = context.getPackageManager();
        Intent intent = new Intent();
        intent.setClassName("com.life.app.client.x10yfirststartconfig",
                "com.life.app.client.x10yfirststartconfig.activities.LoginActivity");
        if (intent == null) {
            return false;
        }
        intent.putExtra(Consts.PARAM_USERNAME, email);
        intent.putExtra(Consts.PARAM_CONFIRMCREDENTIALS, true);
        intent.putExtra(Consts.PARAM_AUTHTOKEN_TYPE, Consts.AUTHTOKEN_TYPE);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        context.startActivity(intent);
        return true;
    }
}
