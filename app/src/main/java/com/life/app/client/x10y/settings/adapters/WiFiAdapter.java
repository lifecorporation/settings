package com.life.app.client.x10y.settings.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.life.app.client.x10y.settings.R;

import java.util.ArrayList;

/*
 * Created by chkalog on 26/8/2016.
 */
public class WiFiAdapter extends BaseAdapter {

    Context context;
    int resource;
    ArrayList<String> data = null;
    SharedPreferences sharedPref;

    public WiFiAdapter(Context context, int resource, ArrayList<String> data) {
        super();
        this.resource = resource;
        this.context = context;
        this.data = data;

        sharedPref = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    }

    @Override
    public int getCount() {
        if(null != this.data){
            return this.data.size();
        }
        return 0;
    }

    @Override
    public String getItem(int arg0) {
        if(null != this.data){
            try {
                return this.data.get(arg0);
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
        }
        return null;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;
        if(row == null){
            LayoutInflater inflater = ((Activity)this.context).getLayoutInflater();
            row = inflater.inflate(this.resource, parent, false);

            holder = new ViewHolder();
            holder.tvTitle  = (TextView)row.findViewById(R.id.text1);
            row.setTag(holder);
        }
        else{
            holder = (ViewHolder)row.getTag();
        }

        String ssid = (String)getItem(position);
        if(ssid!=null) {
            String connected = sharedPref.getString("wifi_ssid", null);
            holder.tvTitle.setText(ssid);
            if (connected != null && ssid.equals(connected)) {
                holder.tvTitle.setTextColor(this.context.getResources().getColor(R.color.green));
            } else {
                holder.tvTitle.setTextColor(this.context.getResources().getColor(R.color.grey_dark));
            }
        }

        return row;
    }

    class ViewHolder{
        TextView tvTitle;
    }
}

