package com.life.app.client.x10y.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.preference.PreferenceFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.life.app.client.x10y.settings.connectivity.WifiActivity;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.BindService;

import com.crashlytics.android.Crashlytics;
import com.life.services.bind.status.Status;
import com.life.services.bind.status.StatusListener;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity{

    BindService bindService = null;

    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.coordinatorLayout, new SettingsFragment())
                    .commit();
        }

        bindService = new BindService(MainActivity.this);
        bindService.registerStatus(new StatusListener() {
            @Override
            public void onStatusReceived(Status newstatus) {
                if(newstatus!=null){
                    //data = intent.getExtras().getByteArray("Data");
                    //Log.i("statusReceiver", Arrays.toString(data));
                    status = newstatus;
                    WifiActivity.update(MainActivity.this);
                }else{
                    status = null;
                    WifiActivity.deviceOFF(MainActivity.this);
                }
            }
        });
        //st2.onReceive(this, null);

        /*timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run(){
                WifiActivity.update(MainActivity.this);
            }
        }, 500, 12000);*/
    }

    @Override
    public void onStart(){
        super.onStart();
    }

    private static Status status = null;
    public static Status getStatus(){
        return status;
    }

    public static class SettingsFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        if(bindService!=null && !bindService.startBindService()) {
            // dobbiamo ancora vedere come gestire la cosa ma attualmente mando solo un messaggio
            // ipoteticamente si potrebbe anche fare partire un servizio che continua a cercare di bindarsi sempre
            Toast.makeText(this,"Non sono riuscito a bindarmi",Toast.LENGTH_LONG).show();
        }
        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        if(bindService!=null){
            bindService.unregisterStatus();
            bindService.unbindService();
            bindService.setmBound(false);
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()){
                        case "-3" :
                            try {
                                Utils.showSnackBar(coordinatorLayout, intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                            }catch (NullPointerException n){
                                n.printStackTrace();
                            }
                            break;
                        default:
                            break;
                    }

                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){

                    /*switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "49" :
                            break;
                        case "50" :
                            break;
                        case "51" :
                            break;
                    }*/
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };
}
