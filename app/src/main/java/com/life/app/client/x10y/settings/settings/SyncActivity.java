package com.life.app.client.x10y.settings.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.messages.Request;

import android.support.design.widget.Snackbar;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/*
 * Created by chkalog on 1/9/2016.
 */
public class SyncActivity extends AppCompatActivity {

    private static Context mContext;
    private static CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sync);

        mContext = SyncActivity.this;
        getFragmentManager().beginTransaction().replace(R.id.coordinatorLayout, new SyncPreferenceFragment()).commit();

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, quitFilter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
    }

    BroadcastReceiver mReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()) {
                        case "97" :
                            startSyncFeedback(mContext.getResources().getString(R.string.error_sync_start));
                            break;
                        case "98" :
                            stopSyncFeedback(mContext.getResources().getString(R.string.error_sync_stop));
                        default:
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "97" :
                            startSyncFeedback(mContext.getResources().getString(R.string.success_sync_start));
                            break;
                        case "98" :
                            stopSyncFeedback(mContext.getResources().getString(R.string.success_sync_stop));
                            break;
                        case "99" :
                            startActivity(new Intent(mContext, SyncStatusActivity.class));
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    public static class SyncPreferenceFragment extends PreferenceFragment implements android.preference.Preference.OnPreferenceClickListener{
        @Override
        public void onCreate(final Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.sync);
            ((Preference) findPreference("start")).setOnPreferenceClickListener(this);
            ((Preference) findPreference("stop")).setOnPreferenceClickListener(this);
            ((Preference) findPreference("status")).setOnPreferenceClickListener(this);
            ((Preference) findPreference("configure")).setOnPreferenceClickListener(this);
        }

        @Override
        public boolean onPreferenceClick(android.preference.Preference preference) {
            String key = preference.getKey();
            switch (key){
                case "status" :
                    Request.syncStatus(mContext);
                    break;
                case "configure" :
                    startActivity(new Intent(mContext, SyncConfigureActivity.class));
                    break;
                case "start" :
                    start();
                    break;
                case "stop" :
                    stop();
                    break;
                case "list" :
                    break;
            }
            return false;
        }
    }

    private static Snackbar startSnackBar = null;
    private static Snackbar stopSnackBar = null;

    private static void start(){
        if(MainActivity.getStatus() != null) {
            startSnackBar = Snackbar
                    .make(coordinatorLayout, mContext.getResources().getString(R.string.prompt_start_sync), Snackbar.LENGTH_LONG);
            startSnackBar.show();
            Request.startSync(mContext);
        }
    }

    private static void stop(){
        if(MainActivity.getStatus() != null) {
            stopSnackBar = Snackbar
                    .make(coordinatorLayout, mContext.getResources().getString(R.string.prompt_stop_sync), Snackbar.LENGTH_LONG);
            stopSnackBar.show();
            Request.stopSync(mContext);
        }
    }

    private static void startSyncFeedback(String msg){
        if(startSnackBar!=null){
            startSnackBar.dismiss();
            Utils.showSnackBar(coordinatorLayout, msg);
        }
    }

    private static void stopSyncFeedback(String msg){
        if(stopSnackBar!=null){
            stopSnackBar.dismiss();
            Utils.showSnackBar(coordinatorLayout, mContext.getResources().getString(R.string.success_sync_stop));
        }
    }
    @Override
    public void onResume(){
        super.onResume();
        if(MainActivity.getStatus() == null)
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
