package com.life.app.client.x10y.settings.account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.messages.Request;

import java.util.Arrays;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/*
 * Created by chkalog on 31/8/2016.
 */
public class LanguageActivity extends AppCompatActivity {

    ListView lvLanguages;
    String lang_code = null;

    static CoordinatorLayout coordinatorLayout;
    ArrayAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_language);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        Request.request(LanguageActivity.this, 0x50, new byte[0]);

        lvLanguages = (ListView) findViewById(R.id.lvLanguages);
        adapter = new ArrayAdapter<String>(this,
                R.layout.language_row,
                getResources().getStringArray(R.array.locale_countries));
        lvLanguages.setAdapter(adapter);
        lvLanguages.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                lang_code = getResources().getStringArray(R.array.locale_codes)[i];
                Request.request(LanguageActivity.this, 0x51, lang_code.getBytes());

            }
        });
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
        if(MainActivity.getStatus() == null)
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE) != null) {
                    try {
                        Utils.showSnackBar(coordinatorLayout, intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                    } catch (NullPointerException n) {
                        n.printStackTrace();
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);
                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "80" :
                            //Update language
                            int index = (Arrays.asList(getResources().getStringArray(R.array.locale_codes))).indexOf(intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_MSG).toString());
                            lvLanguages.setItemChecked(index, true);
                            break;
                        case "81" :
                            //Save language
                            (LanguageActivity.this).finish();
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };
}
