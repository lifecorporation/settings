package com.life.app.client.x10y.settings.consts;

/*
 * Created by chkalog on 5/9/2016.
 */
public class Consts {

    public static final String PARAM_CONFIRMCREDENTIALS   = "confirmCredentials";
    public static final String PARAM_USERNAME             = "username";
    public static final String PARAM_AUTHTOKEN_TYPE       = "authtokenType";

    public static final String DATETIME_FORMAT            = "yyyy-MM-dd'T'HH:mm:ss";

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE               = "com.life.x10y.account.X10YACCOUNT";
    /**
     * Authtoken type string.
     */
    public static final String AUTHTOKEN_TYPE             = "com.life.x10y.account.X10YACCOUNT";

    public static String SERVICE_KEY                      = "com.life.services.bind.messages";
    public static String STATUS_KEY_CONNECT               = "com.app.x10y.telephone.STATUS";
    public static String STATUS_KEY_DISCONNECT            = "com.app.x10y.telephone.DISCONNECTED";

    public static String PREFERENCE_KEY                   = "MyPrefs";
    public static String PREFERENCE_KEY_WIFI_SSID         = "wifi_ssid";
    public static String PREFERENCE_KEY_WIFI_IP           = "wifi_ip";
    public static String PREFERENCE_KEY_WIFI_MODE         = "wifi_mode";
    public static String PREFERENCE_KEY_WIFI_STATUS       = "disconnected";
    public static String PREFERENCE_KEY_WIFI_CONNECTED    = "connected";
    public static String PREFERENCE_KEY_WIFI_DISCONNECTED = "disconnected";
    public static String PREFERENCE_KEY_AUTO_DATETIME     = "automatic";

    public static String PREFERENCE_KEY_ERROR_CODE        = "error_code";
    public static String PREFERENCE_KEY_ERROR_MSG         = "error_msg";

    public static String PREFERENCE_KEY_SUCCESS_CODE      = "success_code";
    public static String PREFERENCE_KEY_SUCCESS_MSG       = "success_msg";

    //Preference keys
    public static final String SAVED_PREF                 = "SAVED_PREF";
    public static final String REGISTRATION_INFO_KEY      = "REGISTRATION_INFO_KEY";
}
