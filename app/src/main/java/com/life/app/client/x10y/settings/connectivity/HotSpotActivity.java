package com.life.app.client.x10y.settings.connectivity;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.utils.Utils;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class HotSpotActivity extends AppCompatActivity {

    TextView tvSwitch;
    SwitchCompat switchView;
    ProgressBar progressBarCircularIndeterminate;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_hot_spot);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        tvSwitch = (TextView) findViewById(R.id.tvSwitch);
        progressBarCircularIndeterminate = (ProgressBar) findViewById(R.id.progressBarCircularIndeterminate);
        switchView = (SwitchCompat) findViewById(R.id.switchView);
        switchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.getStatus() == null){
                    Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
                    switchView.setChecked(false);
                }
            }
        });
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    tvSwitch.setText(getResources().getString(R.string.on));
                }else{
                    progressBarCircularIndeterminate.setVisibility(View.INVISIBLE);
                    tvSwitch.setText(getResources().getString(R.string.off));
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        if(MainActivity.getStatus() == null){
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
            switchView.setEnabled(false);
        }else{
            //switchView.setEnabled(true);
        }
    }
}
