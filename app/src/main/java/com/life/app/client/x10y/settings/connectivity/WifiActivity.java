package com.life.app.client.x10y.settings.connectivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.adapters.WiFiAdapter;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.messages.Request;
import com.life.services.bind.messages.Response;
import com.life.services.bind.objects.WiFi;

import java.util.ArrayList;
import java.util.Arrays;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class WifiActivity extends AppCompatActivity {

    private static ListView lvWifis;
    private static TextView tvSwitch, tvEmpty;
    private static SwitchCompat switchView;
    private static ProgressBar progressBar;

    WiFiAdapter wAdapter;
    public static WiFi wifi = null;
    ArrayList<String> mWiFis = new ArrayList<>();

    SharedPreferences sharedpreferences;
    private static CoordinatorLayout coordinatorLayout;

    private static boolean isCreated = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_wifi);

        sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        lvWifis    = (ListView) findViewById(R.id.lvWifis);
        tvSwitch   = (TextView) findViewById(R.id.tvSwitch);
        tvEmpty    = (TextView) findViewById(R.id.tvEmpty);
        switchView = (SwitchCompat) findViewById(R.id.switchView);
        progressBar = (ProgressBar) findViewById(R.id.progressBarCircularIndeterminate);

        wAdapter = new WiFiAdapter(WifiActivity.this, R.layout.custom_listview_raw_text, mWiFis);
        lvWifis.setAdapter(wAdapter);
        lvWifis.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String connected = sharedpreferences.getString(Consts.PREFERENCE_KEY_WIFI_SSID, null);
                if(connected!=null && mWiFis.get(i).equals(connected))
                    showWifiDialog(sharedpreferences.getString(Consts.PREFERENCE_KEY_WIFI_SSID, null),
                            sharedpreferences.getString(Consts.PREFERENCE_KEY_WIFI_IP, null),
                            sharedpreferences.getString(Consts.PREFERENCE_KEY_WIFI_MODE, null));
                else
                    showConnectDialog(i);
            }
        });

        switchState();

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    Request.request(WifiActivity.this, 0x30, new byte[0]);
                    scan();
                    tvSwitch.setText(getResources().getString(R.string.on));
                }else{
                    hide(WifiActivity.this);
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        isCreated = true;

        if(MainActivity.getStatus()==null){
            deviceOFF(WifiActivity.this);
        }else{
            update(WifiActivity.this);
        }

        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
    }

    private Menu optionsMenu;
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mRefresh:
                if(MainActivity.getStatus() == null){
                    Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
                }else
                    setRefreshActionButtonState(true);
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchState(){
        if(MainActivity.getStatus()==null){
            switchView.setEnabled(false);
            switchView.setChecked(false);
            tvSwitch.setText(getResources().getString(R.string.off));
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
        }else{
            switchView.setEnabled(true);
            if(MainActivity.getStatus().getWifi_status() == 1){
                switchView.setChecked(true);
                tvSwitch.setText(getResources().getString(R.string.on));
            }else{
                switchView.setChecked(false);
                tvSwitch.setText(getResources().getString(R.string.off));
            }
        }
    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.mRefresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                    refresh();
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    private static boolean needsUpdate = true;
    public static void update(Activity mActivity){
        if(isCreated && needsUpdate) {
            needsUpdate = false;
            if (MainActivity.getStatus() != null) {
                //if (MainActivity.getStatus().getWifi_status() == 1) {
                wifiON(mActivity);
                //} else {
                //    wifiOFF(mActivity);
                //}
            }
        }
    }

    private static String TAG = "WiFiActivity";

    public static void wifiON(final Activity mActivity){
        if(isCreated){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switchView.setEnabled(true);
                    switchView.setChecked(true);
                    tvSwitch.setText(mActivity.getResources().getString(R.string.on));
                    wifistatus(mActivity);
                    Request.request(mActivity, 0x30, new byte[0]);
                    scan();
                }
            });
        }
    }

    private void wifiOFF(final Activity mActivity){
        if(isCreated){
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    switchView.setEnabled(true);
                    switchView.setChecked(false);
                    tvSwitch.setText(mActivity.getResources().getString(R.string.off));
                    hide(mActivity);
                }
            });
        }
    }

    public static void deviceOFF(Activity mActivity){
        if(isCreated){
            switchView.setEnabled(false);
            switchView.setChecked(false);
            tvSwitch.setText(mActivity.getResources().getString(R.string.off));
            hide(mActivity);
            Utils.showSnackBar(coordinatorLayout, mActivity.getResources().getString(R.string.warning_remote));
        }
    }

    private void refresh(){
        if(MainActivity.getStatus()==null){
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
            switchView.setEnabled(false);
            tvSwitch.setText(getResources().getString(R.string.off));
        }else{
            switchView.setEnabled(true);
            wifistatus(WifiActivity.this);
            Request.request(WifiActivity.this, 0x30, new byte[0]);
            scan();
        }
    }

    private static void wifistatus(Activity mActivity){
        if(MainActivity.getStatus().getWifi_status()==1){
            switchView.setChecked(true);
            tvSwitch.setText(mActivity.getResources().getString(R.string.on));
        }else{
            switchView.setChecked(false);
            tvSwitch.setText(mActivity.getResources().getString(R.string.off));
        }
    }

    private static void hide(Activity mActivity){
        progressBar.setVisibility(View.INVISIBLE);
        tvSwitch.setText(mActivity.getResources().getString(R.string.off));
        lvWifis.setVisibility(View.GONE);
    }

    private static void scan(){
        progressBar.setVisibility(View.VISIBLE);
        lvWifis.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void stopscan(){
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void error_dialog(String msg){
        Utils.showSnackBar(coordinatorLayout, msg);
        switchView.setChecked(false);
        tvSwitch.setText(getResources().getString(R.string.off));
        lvWifis.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void empty(){
        lvWifis.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.VISIBLE);
    }

    private void notifylist(){
        mWiFis.clear();
        mWiFis.addAll(Response.getWiFiNames());
        wAdapter.notifyDataSetChanged();
        lvWifis.setVisibility(View.VISIBLE);
        tvEmpty.setVisibility(View.GONE);
    }

    private void showWifiDialog(String ssid, String ip, String mode){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(WifiActivity.this);
        final View dialogView = View.inflate(WifiActivity.this, R.layout.dialog_wifi_details, null);
        dialogBuilder.setView(dialogView);
        if(ssid!=null)  dialogBuilder.setMessage(ssid);
        dialogBuilder.setCancelable(false);

        if(ip!=null)    ((TextInputLayout)dialogView.findViewById(R.id.tvIp)).getEditText().setText(ip);
        if(mode!=null)  ((TextInputLayout)dialogView.findViewById(R.id.tvMode)).getEditText().setText(mode);

        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_disconnect), null);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Request.disconnectWifi(WifiActivity.this);
                b.dismiss();
            }
        });
    }

    private void showConnectDialog(final int position){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(WifiActivity.this);
        final View dialogView = View.inflate(WifiActivity.this, R.layout.dialog_wifi_connect, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(mWiFis.get(position));
        dialogBuilder.setCancelable(false);

        final EditText mEtPassword = (EditText)dialogView.findViewById(R.id.etPassword);

        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_connect), null);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_button_cancel), null);
        dialogBuilder.setNeutralButton(getResources().getString(R.string.dialog_button_clear), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
        b.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEtPassword.getText().toString().trim().isEmpty()) {
                    mEtPassword.setError(getResources().getString(R.string.error_password_empty));
                }else{
                    byte[] payload = new byte[0];

                    payload = addElement(payload, (byte)mWiFis.get(position).length());
                    for(byte byte1 : mWiFis.get(position).getBytes())
                        payload = addElement(payload, byte1);

                    payload = addElement(payload, (byte)mEtPassword.getText().toString().length());
                    for(byte byte1 : mEtPassword.getText().toString().getBytes())
                        payload = addElement(payload, byte1);

                    Request.request(WifiActivity.this, 0x31, payload);

                    b.dismiss();
                }
            }
        });

        b.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEtPassword.getText().clear();
            }
        });
    }

    private byte[] addElement(byte[] org, byte added) {
        byte[] result = Arrays.copyOf(org, org.length +1);
        result[org.length] = added;
        return result;
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        isCreated = false;
        needsUpdate = true;
    }

    @Override
    protected void onStop(){
        super.onStop();
        isCreated = false;
        needsUpdate = true;
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, final Intent intent){
            if (intent.getAction().equals(Consts.SERVICE_KEY)){
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE)!=null ) {
                    //Log.i("ERROR_CODE", intent.getExtras().getInt(Consts.PREFERENCE_KEY_ERROR_CODE) + "");
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString()) {
                        case "48":
                            stopscan();
                            setRefreshActionButtonState(false);
                            //Log.i("WIFI_TAG", Consts.PREFERENCE_KEY_ERROR_CODE + " : " + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE));
                            try {
                                error_dialog(intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                            }catch (NullPointerException n){
                                n.printStackTrace();
                            }
                            break;
                        case "49":
                            try {
                                error_dialog(intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                            }catch (NullPointerException n){
                                n.printStackTrace();
                            }
                            break;
                        case "50":
                            try {
                                error_dialog(intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                            }catch (NullPointerException n){
                                n.printStackTrace();
                            }
                            break;
                        default:
                            try {
                                error_dialog(intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString());
                            }catch (NullPointerException n){
                                n.printStackTrace();
                            }
                            break;
                    }

                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);

                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);
                    //Log.i("SUCCESS_CODE", intent.getExtras().getInt(Consts.PREFERENCE_KEY_ERROR_CODE) + "");
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "48" :
                            stopscan();
                            setRefreshActionButtonState(false);

                            Request.request(WifiActivity.this, 0x33, new byte[0]);
                            if(Response.getWiFiNames().isEmpty())
                                empty();
                            else
                                notifylist();
                            break;
                        case "49" :
                            Request.request(WifiActivity.this, 0x33, new byte[0]);
                            notifylist();
                            //RepeatSafeToast.show(WifiActivity.this, "Connected successfully");
                            break;
                        case "50" :
                            clearWiFiPreferences(intent, sharedpreferences.edit());
                            notifylist();
                            break;
                        case "51" :
                            wifi = Response.getWiFi();
                            if(wifi!=null){
                                initiateWiFiPrefrences(intent, sharedpreferences.edit());
                                notifylist();
                                String connected = getSharedPreferences(Consts.PREFERENCE_KEY,
                                        Context.MODE_PRIVATE).getString(Consts.PREFERENCE_KEY_WIFI_SSID, null);
                                if(connected!=null){

                                }
                            }
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    private void clearWiFiPreferences(Intent intent, SharedPreferences.Editor edit){
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_SSID))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_SSID);
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_IP))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_IP);
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_MODE))
            intent.removeExtra(Consts.PREFERENCE_KEY_WIFI_MODE);

        edit.putString(Consts.PREFERENCE_KEY_WIFI_SSID, null);
        edit.putString(Consts.PREFERENCE_KEY_WIFI_IP, null);
        edit.putString(Consts.PREFERENCE_KEY_WIFI_MODE, null);
        edit.apply();
    }

    private void initiateWiFiPrefrences(Intent intent, SharedPreferences.Editor edit){
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_SSID))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_SSID, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_SSID).toString());
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_IP))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_IP, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_IP).toString());
        if(intent.hasExtra(Consts.PREFERENCE_KEY_WIFI_MODE))
            edit.putString(Consts.PREFERENCE_KEY_WIFI_MODE, intent.getExtras().get(Consts.PREFERENCE_KEY_WIFI_MODE).toString());
        edit.apply();
    }
}
