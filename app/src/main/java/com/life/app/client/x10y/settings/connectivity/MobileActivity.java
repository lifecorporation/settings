package com.life.app.client.x10y.settings.connectivity;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.support.v7.widget.SwitchCompat;
import android.widget.TextView;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.utils.Utils;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class MobileActivity extends AppCompatActivity {

    TextView tvSwitch;
    SwitchCompat switchView;
    ProgressBar progressBarCircularIndeterminate;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_mobile);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        tvSwitch = (TextView) findViewById(R.id.tvSwitch);
        progressBarCircularIndeterminate = (ProgressBar) findViewById(R.id.progressBarCircularIndeterminate);
        switchView = (SwitchCompat) findViewById(R.id.switchView);
        switchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(MainActivity.getStatus() == null){
                    Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
                    switchView.setChecked(false);
                }
            }
        });
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    //new TetheringAsync().execute();
                    tvSwitch.setText(getResources().getString(R.string.on));
                }else{
                    progressBarCircularIndeterminate.setVisibility(View.INVISIBLE);
                    tvSwitch.setText(getResources().getString(R.string.off));
                }
            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        if(MainActivity.getStatus() == null){
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, getResources().getString(R.string.warning_remote), Snackbar.LENGTH_LONG);
            snackbar.show();
            switchView.setEnabled(false);
            tvSwitch.setText(getResources().getString(R.string.off));
        }else{
            //switchView.setEnabled(true);
            mobilestatus();
        }
    }

    private void mobilestatus(){
        /*if(MainActivity.getStatus()!=null && MainActivity.getStatus().getMobileStatus()==1){
            switchView.setChecked(true);
            tvSwitch.setText(getResources().getString(R.string.on));
        }else{
            switchView.setChecked(false);
            tvSwitch.setText(getResources().getString(R.string.off));
        }*/
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
