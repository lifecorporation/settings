package com.life.app.client.x10y.settings.account;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.TextClock;
import android.widget.TimePicker;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.messages.Request;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 1/9/2016.
 */
public class DateTimeActivity extends AppCompatActivity {

    static TextInputLayout tvDate, tvTime;
    static CoordinatorLayout coordinatorLayout;
    private static Context mContext;

    SwitchCompat switchView;
    TextClock clock;
    private static int isautomatic = 1;

    SharedPreferences sharedpreferences;
    SharedPreferences.Editor edit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_date_and_time);

        sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();

        mContext = this;
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        Request.getDatetime(this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat dfdate = new SimpleDateFormat("dd MMM yyyy");
        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = dfdate.format(c.getTime());
        String formattedTime = dftime.format(c.getTime());

        tvDate = (TextInputLayout) findViewById(R.id.tvDate);
        tvTime = (TextInputLayout) findViewById(R.id.tvTime);
        if(tvDate.getEditText()!=null)
            tvDate.getEditText().setText(formattedDate);
        //tvTime.getEditText().setText(formattedTime);

        switchView = (SwitchCompat) findViewById(R.id.switchView);
        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    isautomatic = 1;
                    edit.putInt(Consts.PREFERENCE_KEY_AUTO_DATETIME, 1);
                    edit.apply();
                }else{
                    isautomatic = 0;
                    edit.putInt(Consts.PREFERENCE_KEY_AUTO_DATETIME, 0);
                    edit.apply();
                }
            }
        });

        if(sharedpreferences.getInt(Consts.PREFERENCE_KEY_AUTO_DATETIME, -1)==1){
            edit.putInt(Consts.PREFERENCE_KEY_AUTO_DATETIME, 1);
            edit.apply();
            switchView.setChecked(true);
            isautomatic = 1;
        }else{
            edit.putInt(Consts.PREFERENCE_KEY_AUTO_DATETIME, 0);
            edit.apply();
            switchView.setChecked(false);
            isautomatic = 0;
        }

        clock = (TextClock) findViewById(R.id.clock);
    }

    public void setDate(View v){
        if(isautomatic==0) {
            DialogFragment newFragment = new DatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
            if(tvDate.getEditText()!=null) {
                tvDate.getEditText().requestFocusFromTouch();
                tvDate.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    public void setTime(View v){
        if(isautomatic==0) {
            DialogFragment newFragment = new TimePickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
            newFragment.setCancelable(false);
            if(tvTime.getEditText()!=null) {
                tvTime.getEditText().requestFocusFromTouch();
                tvTime.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
            }
        }
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            // If Date and Time retrieved correctly then create Time and Date dialogFragment
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        private String day2 = "";
        private boolean updated = false;
        public void onDateSet(DatePicker view, int year1, int month1, int day1) {
            try {
                day2 = day1+"";
                if(day1<10)  day2 = "0"+day1;
                day   = day1;
                month = month1;
                year  = year1;
                updated = true;
            }catch (NullPointerException n){
                n.printStackTrace();
            }
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            if(tvDate.getEditText()!=null)
                tvDate.getEditText().clearFocus();
        }

        Button positiveButton;

        @Override
        public void onStart() {
            super.onStart();
            AlertDialog d = (AlertDialog) getDialog();
            if (d != null) {
                positiveButton = d.getButton(Dialog.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(updated) {
                            if(tvDate.getEditText()!=null)
                                tvDate.getEditText().setText(day2 + " " + Utils.getMonthName(getActivity(), month) + " " + year);
                            Request.setDatetime(mContext, (byte)isautomatic, generateFormat(year, month, day, hours, minutes));
                        }
                        getDialog().dismiss();
                    }
                });
            }
        }
    }

    private static String generateFormat(int year, int month, int day, int hours, int minutes){

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        //get current date time with Calendar()
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hours, minutes);
        return dateFormat.format(cal.getTime());
    }

    private static int year    = 0;
    private static int month   = 0;
    private static int day     = 0;
    private static int hours   = 0;
    private static int minutes = 0;
    private static int seconds = 0;

    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener{
        @NonNull
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            //Use the current time as the default values for the time picker
            final Calendar c = Calendar.getInstance();
            //hours   = c.get(Calendar.HOUR_OF_DAY);
            //minutes = c.get(Calendar.MINUTE);

            //Create and return a new instance of TimePickerDialog
            return new TimePickerDialog(getActivity(),this, hours, minutes,
                    android.text.format.DateFormat.is24HourFormat(getActivity()));
        }

        //onTimeSet() callback method
        public void onTimeSet(TimePicker view, int hourOfDay, int minute){

            hours   = hourOfDay;
            minutes = minute;

            Log.i("DATETIME", "Hours selected : " + hourOfDay);
            Log.i("DATETIME", "Minutes selected : " + minute);


            if(tvTime.getEditText()!=null)
                tvTime.getEditText().setText(hourOfDay + ":" + minute + ":00");
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            Log.i("DATETIME", "onDismiss, setTime");
            Log.i("DATETIME", day + "/" + month + "/" + year + " " + hours + ":" + minutes);
            Request.setDatetime(mContext, (byte)isautomatic, generateFormat(year, month, day, hours, minutes));
            if(tvTime.getEditText()!=null)
                tvTime.getEditText().clearFocus();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
        if(MainActivity.getStatus() == null)
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    private Menu optionsMenu;
    public boolean onCreateOptionsMenu(Menu menu) {
        this.optionsMenu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mRefresh:
                if(MainActivity.getStatus() == null){
                    Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
                }else
                    setRefreshActionButtonState(true);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setRefreshActionButtonState(final boolean refreshing) {
        if (optionsMenu != null) {
            final MenuItem refreshItem = optionsMenu
                    .findItem(R.id.mRefresh);
            if (refreshItem != null) {
                if (refreshing) {
                    refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
                    Request.getDatetime(this);
                } else {
                    refreshItem.setActionView(null);
                }
            }
        }
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                setRefreshActionButtonState(false);
                if(intent.getExtras()!=null) {
                    if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE) != null) {

                        Log.i("DATETIME", "ERROR_CODE : " + intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE).toString());

                        try {
                            Utils.showSnackBar(coordinatorLayout, intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                        } catch (NullPointerException n) {
                            n.printStackTrace();
                        }
                        intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                        intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);
                    } else if (intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)) {
                        //sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);

                        Log.i("DATETIME", "SUCCESS_CODE : " + intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString());

                        switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()) {
                            case "4":
                                Utils.showSnackBar(coordinatorLayout, "Time updated");

                                convertDate(intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_MSG).toString());

                                clock.setText(intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_MSG).toString());
                                Log.i("DATETIME", intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_MSG).toString());
                                clock.addTextChangedListener(new TextWatcher() {

                                    @Override
                                    public void onTextChanged(CharSequence s, int start, int before,
                                                              int count) {
                                    }

                                    @Override
                                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                                  int after) {
                                    }

                                    @Override
                                    public void afterTextChanged(Editable s) {
                                        if (tvTime.getEditText() != null)
                                            tvTime.getEditText().setText(s.toString());//will be called when system clock updataes

                                    }
                                });

                                break;
                        }
                        intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                        intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                    }
                }
            }
        }
    };

    private void convertDate(String str_date){
        SimpleDateFormat dateFormat = new SimpleDateFormat(Consts.DATETIME_FORMAT);
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(str_date);
            Calendar c = Calendar.getInstance();
            c.setTime(convertedDate);
            day = c.get(Calendar.DAY_OF_MONTH);
            month = c.get(Calendar.MONTH) + 1;
            year = c.get(Calendar.YEAR);

            hours = c.get(Calendar.HOUR_OF_DAY);
            minutes = c.get(Calendar.MINUTE);

            Log.i("DATETIME", hours + ":" + minutes + " " + day + "/" + month + "/" + year);

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
