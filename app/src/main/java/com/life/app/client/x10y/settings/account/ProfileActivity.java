package com.life.app.client.x10y.settings.account;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;

import io.fabric.sdk.android.Fabric;

/*
 * Created by chkalog on 7/9/2016.
 */
public class ProfileActivity extends AppCompatActivity {

    private CoordinatorLayout coordinatorLayout;

    TextInputLayout tvFirstname, tvLastname, tvEmail, tvPassword, tvImei, tvMac;

    private Account mAccount = null;

    private String authToken = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Fabric.with(this, new Crashlytics());

        /**
         * The account manager used to request and add account.
         */
        AccountManager mAccountManager = AccountManager.get(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }else{

            Account[] accountsFromFirstApp = mAccountManager.getAccountsByType(Consts.ACCOUNT_TYPE);
            for(Account acct: accountsFromFirstApp){

                mAccount = acct;

                authToken = mAccountManager.peekAuthToken(acct, Consts.ACCOUNT_TYPE);
                //if(authToken!=null)
                //    Log.i("_ACCOUNT_TOKEN", authToken);
            }
        }

        setContentView(R.layout.activity_profile);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        tvFirstname = (TextInputLayout) findViewById(R.id.tvFirstname);
        tvLastname  = (TextInputLayout) findViewById(R.id.tvLastname);
        tvEmail     = (TextInputLayout) findViewById(R.id.tvEmail);
        tvPassword  = (TextInputLayout) findViewById(R.id.tvPassword);
        tvMac       = (TextInputLayout) findViewById(R.id.tvMac);
        tvImei      = (TextInputLayout) findViewById(R.id.tvImei);

        if(tvPassword.getEditText()!=null)
            tvPassword.getEditText().setTypeface(tvPassword.getEditText().getTypeface(), Typeface.ITALIC);

        setDefault();
        getAccount();
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sync_configure, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save:
                save();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setDefault(){
        if (tvFirstname!=null && tvFirstname.getEditText()!=null) tvFirstname.getEditText().setText("-");
        if (tvLastname!=null  && tvLastname.getEditText()!=null)  tvLastname.getEditText().setText("-");
        if (tvEmail!=null     && tvEmail.getEditText()!=null)     tvEmail.getEditText().setText("-");
        if (tvMac!=null       && tvMac.getEditText()!=null)       tvMac.getEditText().setText("-");
        if (tvImei!=null      && tvImei.getEditText()!=null)      tvImei.getEditText().setText("-");
    }

    private void getAccount() {
        try {
            if (tvEmail!=null && tvEmail.getEditText()!=null && mAccount.name != null)
                tvEmail.getEditText().setText(mAccount.name);
            //if (tvMac!=null && tvMac.getEditText()!=null)   tvMac.getEditText().setText(mac);
            //if (tvImei!=null && tvImei.getEditText()!=null) tvImei.getEditText().setText(imei);
        } catch (SecurityException | ArrayIndexOutOfBoundsException a) {
            setDefault();
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.error_account_not_found));
            a.printStackTrace();
        }
    }

    private void save(){
        if(MainActivity.getStatus() == null){
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
        }
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE) != null) {
                    try {
                        Utils.showSnackBar(coordinatorLayout, intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                    } catch (NullPointerException n) {
                        n.printStackTrace();
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);
                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "0" :
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    public void setFirstname(View v){
        Utils.showSnackBar(coordinatorLayout, "First name");
    }

    public void setLastname(View v){
        Utils.showSnackBar(coordinatorLayout, "Last name");
    }

    public void setEmail(View v){
        if(authToken == null)
            Utils.showSnackBar(coordinatorLayout, "Token Expired");
    }

    public void setPassword(View v){
        if(authToken == null) {
            Utils.showSnackBar(coordinatorLayout, "Token Expired");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Utils.openLoginPage(ProfileActivity.this, mAccount.name);
                }
            }, 1000);
        }else
            editPasswordDialog();
    }

    public void setImei(View v){
        Utils.showSnackBar(coordinatorLayout, "Imei");
    }

    public void setMac(View v){
        Utils.showSnackBar(coordinatorLayout, "Mac");
    }

    /*public class RegistrationInfo {
        public String imei;
        public int userId;
        public String mac;
    }*/

    private void editPasswordDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the
        // dialog layout
        View view = inflater.inflate(R.layout.dialog_profile_edit_password, null);

        TextInputLayout tvOldPassword = (TextInputLayout) view.findViewById(R.id.tvOldPassword);
        final TextInputLayout tvNewPassword = (TextInputLayout) view.findViewById(R.id.tvNewPassword);
        final TextInputLayout tvConfirmPassword = (TextInputLayout) view.findViewById(R.id.tvConfirmPassword);

        builder.setTitle(getResources().getString(R.string.hint_change_password));
        builder.setCancelable(false);
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(getResources().getString(R.string.hint_save), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if(confirmPasswords(tvNewPassword.getEditText().getText().toString(),
                                tvConfirmPassword.getEditText().getText().toString()))
                            AccountManager.get(ProfileActivity.this).setPassword(mAccount,
                                    tvNewPassword.getEditText().getText().toString());
                        else
                            tvConfirmPassword.getEditText().setError(getResources().getString(R.string.error_password_confirmation));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.dialog_button_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        builder.create();
        builder.show();
    }

    private boolean confirmPasswords(String pass1, String pass2){
        if(Utils.isEmpty(pass1) &&
                Utils.isEmpty(pass2))
            if(pass1.equals(pass2))
                return true;
        return false;
    }
}
