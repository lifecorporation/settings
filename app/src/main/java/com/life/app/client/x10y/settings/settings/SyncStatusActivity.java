package com.life.app.client.x10y.settings.settings;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.life.app.client.x10y.settings.R;
import com.life.services.bind.messages.Response;
import com.life.services.bind.objects.SyncStatus;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/*
 * Created by chkalog on 1/9/2016.
 */
public class SyncStatusActivity extends AppCompatActivity {

    SyncStatus status = null;

    TextInputLayout tvSpeed, tvPercentage, tvEstimatedTime, tvCurrentFileNumber, tvFilesCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sync_status);

        status = Response.getSyncStatus();

        tvSpeed             = (TextInputLayout) findViewById(R.id.tvSpeed);
        tvPercentage        = (TextInputLayout) findViewById(R.id.tvPercentage);
        tvEstimatedTime     = (TextInputLayout) findViewById(R.id.tvEstimatedTime);
        tvCurrentFileNumber = (TextInputLayout) findViewById(R.id.tvCurrentFileNumber);
        tvFilesCount        = (TextInputLayout) findViewById(R.id.tvFilesCount);

        tvSpeed.getEditText().setText(status.getSpeed()+"");
        tvPercentage.getEditText().setText(status.getPercentage()+"");
        tvEstimatedTime.getEditText().setText(status.getEstimatedTime()+"");
        tvCurrentFileNumber.getEditText().setText(status.getCurrentFileNumber()+"");
        tvFilesCount.getEditText().setText(status.getFilesCount()+"");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
