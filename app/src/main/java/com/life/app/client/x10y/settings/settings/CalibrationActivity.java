package com.life.app.client.x10y.settings.settings;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.utils.Utils;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/*
 * Created by chkalog on 7/9/2016.
 */
public class CalibrationActivity  extends AppCompatActivity {

    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_calibration);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        Utils.showSnackBar(coordinatorLayout,"Coming soon");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
