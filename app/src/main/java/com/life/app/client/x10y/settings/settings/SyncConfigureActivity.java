package com.life.app.client.x10y.settings.settings;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.life.app.client.x10y.settings.MainActivity;
import com.life.app.client.x10y.settings.R;
import com.life.app.client.x10y.settings.consts.Consts;
import com.life.app.client.x10y.settings.utils.Utils;
import com.life.services.bind.messages.Request;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
/*
 * Created by chkalog on 2/9/2016.
 */
public class SyncConfigureActivity extends AppCompatActivity{

    TextInputLayout tvAutomatic, tvConnectionType, tvPowerStatus, tvBatteryLimit, tvTimeout;

    private boolean automatic = false;
    private String connection = null, powersupply = null;
    private int connection_id = 0, powersupply_id = 0;
    private int batterylimit = 0;
    private int timeout = 0;

    static CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_sync_configure);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        if(connection == null) {
            connection = getResources().getString(R.string.hint_wifi);
            connection_id = R.id.rbWifi;
        }

        if(powersupply == null) {
            powersupply = getResources().getString(R.string.hint_charging);
            powersupply_id = R.id.rbCharging;
        }

        tvAutomatic      = (TextInputLayout) findViewById(R.id.tvAutomatic);
        tvConnectionType = (TextInputLayout) findViewById(R.id.tvConnectionType);
        tvPowerStatus    = (TextInputLayout) findViewById(R.id.tvPowerStatus);
        tvBatteryLimit   = (TextInputLayout) findViewById(R.id.tvBatteryLimit);
        tvTimeout        = (TextInputLayout) findViewById(R.id.tvTimeout);

        tvAutomatic.getEditText().setText(automatic+"");
        tvConnectionType.getEditText().setText(connection);
        tvPowerStatus.getEditText().setText(powersupply);
        tvBatteryLimit.getEditText().setText(batterylimit + "%");
        tvTimeout.getEditText().setText(timeout + getResources().getString(R.string.prompt_second));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sync_configure, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save:
                save();
                return true;
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void save(){
        if(MainActivity.getStatus() == null){
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
        }else {
            byte automatic_byte = 0x00;
            byte connection_byte = 0x00;
            byte power_byte = 0x00;

            if (automatic) automatic_byte = 0x01;

            if (connection.equals(getString(R.string.hint_4G))) connection_byte = 0x01;
            else if (connection.equals(getString(R.string.hint_both))) connection_byte = 0x02;

            if (powersupply.equals(getString(R.string.hint_battery))) power_byte = 0x01;

            Request.configureSync(SyncConfigureActivity.this, automatic_byte, connection_byte, power_byte, batterylimit, timeout);
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        IntentFilter quitFilter = new IntentFilter();
        quitFilter.addAction(Consts.SERVICE_KEY);
        LocalBroadcastManager.getInstance(this).registerReceiver(m_quitReceiver, quitFilter);
        if(MainActivity.getStatus() != null)
            Utils.showSnackBar(coordinatorLayout, getResources().getString(R.string.warning_remote));
    }

    @Override
    public void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(m_quitReceiver);
    }

    BroadcastReceiver m_quitReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, final Intent intent) {
            if (intent.getAction().equals(Consts.SERVICE_KEY)) {
                if (intent.hasExtra(Consts.PREFERENCE_KEY_ERROR_CODE) && intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_CODE) != null) {
                    try {
                        Utils.showSnackBar(coordinatorLayout, intent.getExtras().get(Consts.PREFERENCE_KEY_ERROR_MSG).toString());
                    } catch (NullPointerException n) {
                        n.printStackTrace();
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_ERROR_MSG);
                }else if(intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE) && intent.hasExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG)){
                    //sharedpreferences = getSharedPreferences(Consts.PREFERENCE_KEY, Context.MODE_PRIVATE);
                    switch (intent.getExtras().get(Consts.PREFERENCE_KEY_SUCCESS_CODE).toString()){
                        case "100" :
                            Utils.showSnackBar(coordinatorLayout, "Configuration was completed successfully!");
                            break;
                    }
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_CODE);
                    intent.removeExtra(Consts.PREFERENCE_KEY_SUCCESS_MSG);
                }
            }
        }
    };

    public void setAutomatic(View v){
        tvAutomatic.getEditText().requestFocus();
        tvAutomatic.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_automatic, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_automatic));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setNegativeButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvAutomatic.getEditText().getBackground().clearColorFilter();
                tvAutomatic.getEditText().clearFocus();
                tvAutomatic.getEditText().setText(automatic+"");
                dialogInterface.dismiss();
            }
        });

        ((SwitchCompat)dialogView.findViewById(R.id.switchView)).setChecked(automatic);
        ((SwitchCompat)dialogView.findViewById(R.id.switchView)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                automatic = b;
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setConnection(View v){
        tvConnectionType.getEditText().requestFocus();
        tvConnectionType.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_connection, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_connectiontype));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvConnectionType.getEditText().setText(connection);
                tvConnectionType.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                connection = ((RadioButton)dialogView.findViewById(i)).getText().toString();
                connection_id = i;
            }
        });

        if(connection==null) {
            ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).check(0);
            connection = getResources().getString(R.string.hint_wifi);
        }else{
            ((RadioGroup)dialogView.findViewById(R.id.rgConnection)).check(connection_id);
        }

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setPowerSupply(View v){
        tvPowerStatus.getEditText().requestFocus();
        tvPowerStatus.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_power, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_powerstatus));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvPowerStatus.getEditText().setText(powersupply);
                tvPowerStatus.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        ((RadioGroup)dialogView.findViewById(R.id.rgPower)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                powersupply = ((RadioButton)dialogView.findViewById(i)).getText().toString();
                powersupply_id = i;
            }
        });

        if(powersupply==null) {
            ((RadioGroup)dialogView.findViewById(R.id.rgPower)).check(0);
            powersupply = getResources().getString(R.string.hint_wifi);
        }else{
            ((RadioGroup)dialogView.findViewById(R.id.rgPower)).check(powersupply_id);
        }

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setBatteryLimit(View v){
        tvBatteryLimit.getEditText().requestFocus();
        tvBatteryLimit.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_battery, null);

        final SeekBar seekBar = (SeekBar) dialogView.findViewById(R.id.seekBar1);
        final TextView textView = (TextView) dialogView.findViewById(R.id.textView1);
        // Initialize the textview with '0'.
        textView.setText(batterylimit + "%");
        seekBar.setProgress(batterylimit);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                batterylimit = seekBar.getProgress();
                textView.setText(batterylimit + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_powerstatus));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tvBatteryLimit.getEditText().setText(seekBar.getProgress() + "%");
                tvBatteryLimit.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void setTimeout(View v){
        tvTimeout.getEditText().requestFocus();
        tvTimeout.getEditText().getBackground().setColorFilter(getResources().getColor(R.color.grey_dark), PorterDuff.Mode.SRC_ATOP);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SyncConfigureActivity.this);
        final View dialogView = View.inflate(SyncConfigureActivity.this, R.layout.dialog_sync_configure_timeout, null);

        final EditText etTimeout = (EditText) dialogView.findViewById(R.id.etTimeout);

        dialogBuilder.setView(dialogView);
        dialogBuilder.setMessage(getResources().getString(R.string.hint_timeout));
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_button_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(etTimeout.getText().toString().trim().isEmpty())
                    timeout = 0;
                else
                    timeout = Integer.parseInt(etTimeout.getText().toString());

                tvTimeout.getEditText().setText(timeout + getResources().getString(R.string.prompt_second));
                tvTimeout.getEditText().clearFocus();
                dialogInterface.dismiss();
            }
        });

        final AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
